module API
  module V1
    class Singers < Grape::API
      include API::V1::Defaults

      resource :singers do
        desc "Return all singers"
        get "", root: :singers do
          Singer.all
        end

        desc "Return a singer"
        params do
          requires :id, type: String, desc: "ID of the singer"
        end
        get ":id", root: "singer" do
          Singer.where(id: permitted_params[:id]).first!
        end
      end
    end
  end
end
