class Album < ActiveRecord::Base
  belongs_to :singer
  validates :name,
    presence: true
end
